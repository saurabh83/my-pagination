import UserSidebar from "./UserSidebar";
import axios from "axios";
import fileDownload from "js-file-download";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAngleRight,
  faFile,
  faSearch,
} from "@fortawesome/free-solid-svg-icons";
import {
  faMobileScreen,
  faEnvelope,
  faDownload,
  faMessage,
} from "@fortawesome/free-solid-svg-icons";
import {
  faWhatsapp,
  faFacebookMessenger,
} from "@fortawesome/free-brands-svg-icons";
import { useState, useEffect } from "react";
import parse from "html-react-parser";
import Link from "next/link";

function AssignmentCart() {

  var sts = ["Waiting", "Order Placed", "Preview", "Completed", "Rejected", "Cancelled", "Deleted"]

  const [search, setsearch] = useState('')

  const [errsearch, seterrsearch] = useState('')

  const [projectData, setprojectData] = useState([]);
  const [singleProject, setsingleProject] = useState(null);

  const [number, setNumber] = useState(1); // No of pages
  const [postPerPage] = useState(10);

  const lastPost = number * postPerPage;
  const firstPost = lastPost - postPerPage;
  const currentPost = projectData.slice(firstPost, lastPost);
  const pageNumber = [];

  var token = ""
  if (typeof window !== "undefined") {
    token = localStorage.getItem('token');
  }

  const filterProjectList = async (doc) => {
    if (doc === "") {
      return;
    }
    else {
      setprojectData([])
      seterrsearch("")
      console.log("DOcumnet id: ", doc)
      await axios.get(`${process.env.NEXT_APP_YAH_API}/project/${doc}`, {
        headers: {
          authorization: `Bearer ${token}`
        }
      })
        .then(result => {
          console.log("data get : ", result.data)
          setsingleProject([result.data]);

          console.log('data :', singleProject)
        })
        .catch(error => {
          seterrsearch(error.response.data.message)
          if (error.response.status + "" === "404") {
            seterrsearch("Search not found!!!!")
          }
          console.log("errooorr", error)
        })
    }
  }
  useEffect(() => {
    const fetchProjectList = async () => {
      await axios.get(`${process.env.NEXT_APP_YAH_API}/project`, {
        headers: {
          authorization: `Bearer ${token}`
        }
      })
        .then(result => {
          console.log("result data ", result.data)
          const resultData = result.data

          setprojectData(resultData)
        })
        .catch(error => {
          console.log("errooorr", error)
        })
    }
    fetchProjectList();
  }, [token]);



  const downloadDocs = async (file_name, doc) => {

    axios.get(`${process.env.NEXT_APP_YAH_API}/project/getFile/${doc}`,
      {
        responseType: 'blob',
      }
    )
      .then((result) => {
        fileDownload(result.data, file_name);
      })
      .catch(error => {
        console.log("error", error)
      });
  }


  for (let i = 1; i <= Math.ceil(projectData.length / postPerPage); i++) {
    pageNumber.push(i);
  }

  const ChangePage = (pageNumber) => {
    setNumber(pageNumber);
  };


  const chunk = (arr, size) => {
    const result = [];
    let i = 0;
    while (i < arr.length) {
      result.push(arr.slice(i, i += size));
    }
    return result;
  }

  const chunks = chunk(pageNumber, 5)
  const pageThis = chunks[Math.floor((number-1) / 5)]


  if (typeof window !== "undefined") {

    console.log('first::', firstPost);
    if (firstPost == 0) {
      hidePrev()
    }
    else if (pageNumber.length == lastPost / postPerPage) {
      hideNext()
    }
    else {
      document.getElementById('next-button').style.visibility = 'visible';
      document.getElementById('prev-button').style.visibility = 'visible';

    }
  }
  if (typeof window !== "undefined") {
    if (lastPost <= postPerPage && chunks.length == 1) {
      document.getElementById('next-button').style.visibility = 'hidden';
      document.getElementById('prev-button').style.visibility = 'hidden';

    }
  }

  //condition for previous button disable
  function hidePrev() {

    if (lastPost / postPerPage - 1 < 2) {
      document.getElementById('prev-button').style.visibility = 'hidden';
    }
    else {
      document.getElementById('prev-button').style.visibility = 'visible';
    }
    if (lastPost / postPerPage >= 1) {
      document.getElementById('next-button').style.visibility = 'visible';
    }
  }


  // condition for next button diable
  function hideNext() {
    if (lastPost >= projectData.length - 1) {
      document.getElementById('next-button').style.visibility = 'hidden';
    }
    else {
      document.getElementById('next-button').style.visibility = 'visible';
    }

    if (lastPost / postPerPage >= 1) {
      document.getElementById('prev-button').style.visibility = 'visible';
    }
  }

  // console.log('prev:', lastPost, '==', postPerPage, '&&', currentPost.length, '==', postPerPage)
  // console.log('next:', lastPost, '>=', projectData.length, '&&', currentPost.length, '=<', postPerPage)


  return (
    <div>
      <div className="md:flex">
        <div className="md:w-1/4 mt">
          <UserSidebar />
        </div>
        <div className="md:w-3/4 bg-sky-50 py-2 px-3 md:pt-8">
          <div>
            {/* <Searchbar /> */}
          </div>
          <div className="p-3">
            <div className="md:py-4">
              <div className="text-center md:text-2xl text-lg font-semibold">
                MY PROJECTS
              </div>
              {/* search bar */}
              <div className="md:px-52">
                <form>
                  <div className="shadow-md shadow-gray-500">
                    <div className="flex">
                      <div className="bg-white flex justify-center items-center px-1.5 md:px-4 border border-r-0 border-gray-300">
                        <FontAwesomeIcon
                          icon={faSearch}
                          className="text-gray-500 md:text-xl"
                        />
                      </div>
                      <input
                        type="search"
                        value={search}
                        onChange={(e) => {
                          setsearch(e.target.value);
                        }}
                        name=""
                        id=""
                        className="w-full border border-gray-300 border-r-0 border-l-0 py-2 md:py-3 px-1 focus:outline-none"
                        placeholder="Search Via Order ID"
                      />
                      <div className="border border-gray-300 border-l-0 md:px-2 flex justify-center items-center bg-white">
                        <button
                          onClick={(e) => {
                            e.preventDefault();
                            filterProjectList(search);
                          }}
                          type="submit"
                          className="bg-sky-700 hover:bg-gradient-to-l from-sky-500 to-sky-900 text-white px-5 py-[9px] rounded-sm"
                        >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              {/* search bar end */}
              {errsearch ? (
                <span style={{ color: "crimson" }}>{errsearch}</span>
              ) : (
                ""
              )}
              {/* <ul className='flex space-x-9 font-semibold'>
                <li>Page: 1{projects1?.page}</li>
                <li>Page Size: {projects1?.pageSize}</li>
                <li>Total: {projects1?.total}</li>
                <li>Total Pages: {projects1?.totalPages}</li>
              </ul> */}
              {/* <div className='mt-2'><div className='bg-gray-200 h-0.5 '></div></div> */}

              {/* project table */}
              <div className="relative my-5 md:my-8 md:mb-20 overflow-x-auto rounded-sm">
                <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                  <thead className="text-xs text-white uppercase bg-sky-900 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                      <th scope="col" className="px-4 py-3">
                        Order Id
                      </th>
                      <th scope="col" className="px-4 py-3">
                        Title
                      </th>
                      <th scope="col" className="px-4 py-3">
                        Subject
                      </th>
                      <th scope="col" className="px-4 py-3">
                        Question
                      </th>
                      <th scope="col" className="px-4 py-3">
                        Uploaded On
                      </th>
                      <th scope="col" className="px-4 py-3">
                        Deadline Date
                      </th>
                      <th scope="col" className="px-4 py-3">
                        File Download
                      </th>
                      <th scope="col" className="px-4 py-3">
                        Price
                      </th>
                      <th scope="col" className="px-4 py-3">
                        Status
                      </th>

                      {/* <th scope="col" className="px-4 py-3">
                        View
                      </th> */}
                    </tr>
                  </thead>
                  <tbody>
                    {currentPost && currentPost.map((pro, i) => {
                      if (singleProject == null) {
                        return (
                          <>

                            <tr className="bg-white border-b odd:bg-sky-100 even:bg-white dark:bg-gray-800 dark:border-gray-700">
                              <th
                                scope="row"
                                className="px-6 py-4 font-medium text-gray-900 dark:text-black whitespace-nowrap"
                              >
                                {pro?.orderNumber}
                              </th>
                              <td className="px-4 py-4">{pro?.title}</td>
                              <td className="px-4 py-4">{pro?.subject}</td>
                              <td className="px-4 py-4">{parse(pro?.questionText + "")} </td>
                              <td className="px-4 py-4">
                                {new Date(pro?.postedDate).toLocaleDateString()}
                              </td>
                              <td className="px-4 py-4">
                                {new Date(pro?.deadline).toLocaleDateString()}
                              </td>
                              <td className="px-4 py-4 flex space-x-3">{pro?.files?.map((fil, i) => {
                                return <>
                                  <div className="relative flex flex-col items-center group">
                                    <FontAwesomeIcon icon={faFile} className="text-2xl text-sky-800 hover:text-slate-800 mx-auto cursor-pointer" onClick={() => { downloadDocs(fil.original_name, fil.id) }} />
                                    <div className="absolute bottom-0  flex-col items-center hidden mb-6 group-hover:flex">
                                      <span className="relative z-auto rounded-full w-48 px-4 py-2 text-sm leading-none text-black font-semibold whitespace-no-wrap break-all bg-white shadow-lg shadow-gray-800">{fil.original_name}</span>
                                      <div className="w-4 h-4 -mt-2 rotate-45 bg-white"></div>
                                    </div>
                                  </div></>
                              })}
                              </td>
                              <td className="px-4 py-4">$10</td>
                              <td className="px-4 py-4">waiting</td>


                              {/* <td className="px-4 py-4"><Link href={{
                                pathname: "/userpanel/View",
                                query: { id: pro.id },
                              }}><a>View</a>
                              </Link></td> */}
                            </tr>
                          </>
                        );
                      }
                    })}
                    {singleProject && singleProject.map((pro, i) => {
                      return <>

                        <tr className="bg-white border-b odd:bg-sky-100 even:bg-white dark:bg-gray-800 dark:border-gray-700">
                          <th
                            scope="row"
                            className="px-6 py-4 font-medium text-gray-900 dark:text-black whitespace-nowrap"
                          >
                            {pro?.orderNumber}
                          </th>
                          <td className="px-4 py-4">{pro?.title}</td>
                          <td className="px-4 py-4">{pro?.subject}</td>
                          <td className="px-4 py-4"> {parse(pro?.questionText + "")}</td>
                          <td className="px-4 py-4">
                            {new Date(pro?.postedDate).toLocaleDateString()}
                          </td>
                          <td className="px-4 py-4">
                            {new Date(pro?.deadline).toLocaleDateString()}
                          </td>
                          <td className="px-4 py-4">
                            {pro?.files?.map((fil, i) => {
                              return (
                                <>
                                  <FontAwesomeIcon
                                    icon={faFile}
                                    className="text-lg hover:text-blue-600 mx-auto cursor-pointer"
                                    onClick={() => {
                                      downloadDocs(
                                        fil.original_name,
                                        fil.id
                                      );
                                    }}
                                  />
                                  {fil.original_name}
                                  <br />
                                </>
                              );
                            })}
                          </td>
                          {/* <td className="px-4 py-4">Submitted</td>
                    <td className="px-4 py-4 mx-auto"><FontAwesomeIcon icon={faMessage} className="text-lg hover:text-blue-600 mx-auto cursor-pointer"/></td>
                    <td className="px-4 py-4 text-center"><FontAwesomeIcon icon={faDownload} className="text-lg hover:text-blue-600 mx-auto cursor-pointer"/> </td> */}
                        </tr>
                      </>
                    })}
                  </tbody>
                </table>
              </div>
              <div className="my-3 text-center">
                <button id="prev-button"
                  className="px-3 py-1 m-1 text-center btn-primary"
                  onClick={() => { setNumber(number - 1); hidePrev() }}
                >
                  Previous
                </button>

                {

                  pageThis?.map((Elem) => {
                    return (
                      <>
                        <button
                          className={`${number === Elem ? 'px-3 py-1 m-1 text-center textcolor' : 'px-3 py-1 m-1 text-center lightcolor'}`}
                          onClick={() => ChangePage(Elem)}
                        >
                          {Elem}
                        </button>
                      </>
                    );
                  })

                }
                <button id="next-button"
                  className="px-3 py-1 m-1 text-center btn-primary"
                  onClick={() => { setNumber(number + 1); hideNext() }}
                >
                  Next
                </button>
              </div>

              <div>
                <div className="flex md:flex-row flex-col justify-center items-center">
                  <img
                    src="/images/newhome/party.gif"
                    className="w-24 h-24"
                    alt="gifs"
                  />
                  <div className="my-auto text-center mb-3">
                    <p className="flex-grow sm:pr-16 text-lg md:text-2xl font-medium title-font text-gray-900">
                      Congratulations for joining 60,000+ students
                    </p>
                    <p>
                      Brief your requirement expert to get the best quote to
                      secure high grades
                    </p>
                  </div>
                </div>
                <div className="my-5 text-center">
                  <button className="bg-orange-600 text-white px-14 py-2 rounded-md">
                    Chat Now
                  </button>
                </div>
              </div>

              {/* connect with us section */}
              <div className="mt-20"></div>
              <div className="text-center md:text-xl text-lg font-semibold">
                Connect With Us
              </div>
              <div className="mt-2">
                <div className="bg-gray-200 h-0.5 "></div>
              </div>
              <div className="md:flex md:space-x-5 mt-5">
                <div className="md:w-1/4 text-center">
                  <div className="text-4xl py-4 text-sky-600">
                    <FontAwesomeIcon icon={faFacebookMessenger} />
                  </div>
                  <div className="text-base font-semibold">
                    Chat on Messenger
                  </div>
                </div>

                <div className="md:w-1/4 text-center">
                  <div className="text-4xl py-4 text-emerald-700">
                    <FontAwesomeIcon icon={faWhatsapp} />
                  </div>
                  <div className="text-base font-semibold">
                    Chat on Whatsapp
                  </div>
                </div>
                <div className="md:w-1/4 text-center">
                  <div className="text-4xl py-4">
                    <FontAwesomeIcon icon={faMobileScreen} />
                  </div>
                  <div className="text-base font-semibold">Call Us</div>
                  <div className="py-2">+1-617-874-1011(US)</div>
                </div>
                <div className="md:w-1/4 text-center">
                  <div className="text-4xl py-4 text-rose-600">
                    <FontAwesomeIcon icon={faEnvelope} />
                  </div>
                  <div className="text-base font-semibold">Mail Us</div>
                  <div className="py-2">support@assignmenthelp.net</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AssignmentCart;
